package behaviours.exo3;

import java.util.Random;

import jade.core.AID;
import jade.core.Agent;
import jade.core.behaviours.SimpleBehaviour;
import jade.lang.acl.ACLMessage;
import jade.lang.acl.MessageTemplate;


/***
 * This behaviour allows the agent who possess it to send nb random int within [0-100[ to another agent whose local name is given in parameters
 * 
 * There is not loop here in order to reduce the duration of the behaviour (an action() method is not preemptive)
 * The loop is made by the behaviour itslef
 * 
 * @author Cédric Herpson
 *
 */

public class SendValuesBehaviour extends SimpleBehaviour{
	
	private static final long serialVersionUID = 9088209402507795289L;

	private boolean finished=false;

	
	/**
	 * number of values already sent
	 */
	private int nbMessagesSent=0;
	
	/**
	 * Name of the agent that should receive the values
	 */
	private String receiverName;
	
	Random r;

	/**
	 * 
	 * @param myagent the Agent this behaviour is linked to
	 * @param nbValues the number of messages that should be sent to the receiver
	 * @param receiverName The local name of the receiver agent
	 */
	public SendValuesBehaviour(final Agent myagent, String receiverName) {
		super(myagent);
		this.receiverName=receiverName;
		this.r= new Random();
		
	}


	public void action() {
			
		//1) receive the message
		final MessageTemplate msgTemplate = MessageTemplate.and(MessageTemplate.MatchPerformative(ACLMessage.REQUEST),
				MessageTemplate.MatchSender(new AID(this.receiverName, AID.ISLOCALNAME)));

		final ACLMessage msgReceived = this.myAgent.receive(msgTemplate);
		if (msgReceived != null && msgReceived.getContent() != "STOP") {		
			System.out.println(this.myAgent.getLocalName()+"<----Request received from "+msgReceived.getSender().getLocalName()+" ,content= "+msgReceived.getContent());
			
			final ACLMessage msg = new ACLMessage(ACLMessage.INFORM);
			msg.setSender(this.myAgent.getAID());
			msg.addReceiver(new AID(this.receiverName, AID.ISLOCALNAME));  
			
			//2° compute the random value		
			msg.setContent(((Integer)r.nextInt(100)).toString());
			this.myAgent.send(msg);
			nbMessagesSent++;
			System.out.println(this.myAgent.getLocalName()+" ----> Message number "+this.nbMessagesSent+" sent to "+this.receiverName+" ,content= "+msg.getContent());
			
			
		}
		if(msgReceived.getContent() == "STOP"){
			this.finished=true; // After the execution of the action() method, this behaviour will be erased from the agent's list of triggerable behaviours.
		}
		else {
			block();
		}
	}

	public boolean done() {
		return finished;
	}

}
