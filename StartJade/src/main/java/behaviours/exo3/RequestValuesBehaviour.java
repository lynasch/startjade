package behaviours.exo3;

import jade.core.AID;
import jade.core.Agent;
import jade.core.behaviours.SimpleBehaviour;
import jade.lang.acl.ACLMessage;
import jade.lang.acl.MessageTemplate;

public class RequestValuesBehaviour extends SimpleBehaviour {

	/**
	 * 
	 */
	private static final long serialVersionUID = 4604283880722399759L;
	private boolean finished=false;
	
	/**
	 * Number of int to receive
	 */
	private int nbValues;
	
	/**
	 * Number of int received until now
	 */
	private int nbMessagesReceived;

	/**
	 * Name of the agent to send the result back
	 */
	private String senderName="";
	
	/**
	 * sum
	 */
	private int sum;
	
	public RequestValuesBehaviour(final Agent myagent, int nbValues, String resultReceiver) {
		super(myagent);
		this.nbValues=nbValues;
		this.nbMessagesReceived=0;
		this.senderName=resultReceiver;
		this.sum=0;

	}
	
	@Override
	public void action() {

		//1) Send a request value
		final ACLMessage msgRequestValue = new ACLMessage(ACLMessage.REQUEST);
		msgRequestValue.setSender(this.myAgent.getAID());
		msgRequestValue.addReceiver(new AID(this.senderName, AID.ISLOCALNAME));  	
		msgRequestValue.setContent("REQUEST_VALUE");
		this.myAgent.send(msgRequestValue);
		System.out.println("REQUEST VALUE FROM "+this.senderName);
		
		//2) create the reception template (inform + name of the sender)
		final MessageTemplate msgTemplate = MessageTemplate.and(MessageTemplate.MatchPerformative(ACLMessage.INFORM),
																		MessageTemplate.MatchSender(new AID(this.senderName, AID.ISLOCALNAME)));

		
		//3) add the value to the counter and send a ACK message
		if (this.nbMessagesReceived < this.nbValues ) {
			final ACLMessage msg = this.myAgent.receive(msgTemplate);
			if (msg != null) {		
				System.out.println(this.myAgent.getLocalName()+"<----Result received from "+msg.getSender().getLocalName()+" ,content= "+msg.getContent());
				this.sum=this.sum+(Integer.parseInt(msg.getContent()));
				this.nbMessagesReceived++;
				
				final ACLMessage msgRequestMReceived = new ACLMessage(ACLMessage.REQUEST);
				msgRequestMReceived.setSender(this.myAgent.getAID());
				msgRequestMReceived.addReceiver(new AID(this.senderName, AID.ISLOCALNAME));  	
				msgRequestMReceived.setContent("MESSAGE_RECEIVED");
				this.myAgent.send(msgRequestMReceived);
				System.out.println("MESSAGE RECEIVED FROM "+this.senderName);
			}
		}

		//3) Send a request stop
		if (this.nbValues == this.nbMessagesReceived) {
			final ACLMessage msgRequestStop = new ACLMessage(ACLMessage.REQUEST);
			msgRequestStop.setSender(this.myAgent.getAID());
			msgRequestStop.addReceiver(new AID(this.senderName, AID.ISLOCALNAME));  	
			msgRequestStop.setContent("STOP");
			this.myAgent.send(msgRequestStop);
			System.out.println("REQUEST STOP FROM "+this.senderName+"\n Sum computed and equals "+this.sum);
			this.finished = true;
		}else {
			block();
		}
	}

	@Override
	public boolean done() {
		return finished;
	}

}
