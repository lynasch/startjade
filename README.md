# startJade

This program's goal is to give you a way to launch your JADE program without using the jade's GUI.


Indeed, using the [JADE](http://jade.tilab.com)'s documentation, you can hardly find how to launch a platform from source code. This project only aims is thus to show you how simple it is to create and launch some agents with jade from the code through a few examples.


This is a java and maven project packaged for Eclipse.

## Three examples and a half are currently available :

 - Half-example : How to create a platform
 
 - Example 1 : How to create agents and send a message
 
   - Agent0 will send one message to agent1, that's all :)
  
 - Example 2 : How to exchange messages between two agents
 
   - AgentA will send 10 random values to agentSUM, which will compute the sum and return the result to agentA.

 - Example 3 : How to migrate from one container to another (intraplatform)
 
   - Agent Oscillator moves from container2 to container 1 then oscillates for eternity between containers 1 and 3. It does nothing else.

## Download the project

 - Using git : git clone https://gitlab.com/herpsonc/startJade.git (or through your ide)

 - Or downloading the archive from this page : https://gitlab.com/herpsonc/startJade

## Install and execute it

See the [wiki](https://gitlab.com/herpsonc/startJade/wikis/home) for installation and execution details


## License

This program is Free Software: You can use, study share and improve it at your
will. Specifically you can redistribute and/or modify it under the terms of the GNU Lesser General Public License, either version 3 or any later version.


> Cédric Herpson, version 14/05/2017, 
> Written with [StackEdit](https://stackedit.io/).
